package com.kitegamesstudio.assignment.mashup;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.kbeanie.multipicker.api.callbacks.AudioPickerCallback;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenAudio;
import com.kbeanie.multipicker.api.entity.ChosenVideo;
import com.kitegamesstudio.assignment.mashup.fs.FilePicker;
import com.kitegamesstudio.assignment.mashup.fs.Path;
import com.kitegamesstudio.assignment.mashup.notification.NotificationCenter;
import com.kitegamesstudio.assignment.mashup.services.ProcessingIntentService;
import java.util.List;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;


@RuntimePermissions
public class MainActivity extends AppCompatActivity implements View.OnClickListener {



    private EditText txtOutputFile;
    private TextView [] txtFileNames;
    private String [] filePaths = new String[3];

    private FilePicker filePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initUi();

        loadFFMpeg();

        filePicker = new FilePicker(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivityPermissionsDispatcher.execFFMpegWithPermissionCheck(MainActivity.this);
            }
        });



    }

    /**
     * Getting the ui views
     */
    private void initUi(){
        txtFileNames = new TextView[3];
        txtFileNames[0] = (TextView)findViewById(R.id.txt_first_file);
        txtFileNames[1] = (TextView)findViewById(R.id.txt_second_file);
        txtFileNames[2] = (TextView)findViewById(R.id.txt_audio_file);

        txtOutputFile = (EditText)findViewById(R.id.output);

        findViewById(R.id.btn_import_first).setOnClickListener(this);
        findViewById(R.id.btn_import_second).setOnClickListener(this);
        findViewById(R.id.btn_import_audio).setOnClickListener(this);
    }

    /**
     * Click events for this activity
     * @param view (the view that is being clicked)
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_import_first:
                pickVideo(0);
                break;
            case R.id.btn_import_second:
                pickVideo(1);
                break;
            case R.id.btn_import_audio:
                pickAudio(2);
                break;
        }
    }

    /**
     * Event for picking video
     * @param index ()
     */
    private void pickVideo(final int index){
        filePicker.pickVideo(new VideoPickerCallback() {
            @Override
            public void onVideosChosen(List<ChosenVideo> list) {
                if (list != null && list.size() > 0) {
                    txtFileNames[index].setText(new Path(list.get(0).getOriginalPath()).getFileName());
                    filePaths[index] = list.get(0).getOriginalPath();
                }
            }

            @Override
            public void onError(String s) {

            }
        });
    }

    /**
     * Event for picking audio
     * @param index ()
     */
    private void pickAudio(final int index){
        filePicker.pickAudio(new AudioPickerCallback() {
            @Override
            public void onAudiosChosen(List<ChosenAudio> list) {
                if (list != null && list.size() > 0) {
                    txtFileNames[index].setText(new Path(list.get(0).getOriginalPath()).getFileName());
                    filePaths[index] = list.get(0).getOriginalPath();
                }
            }

            @Override
            public void onError(String s) {

            }
        });
    }


    /**
     * Loading the ffmpeg binary
     * this may take some time
     *
     * TODO: move it to splash
     */
    private void loadFFMpeg(){
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                    System.out.println("load binary started");
                }

                @Override
                public void onFailure() {
                    System.out.println("load binary failed !!");
                }

                @Override
                public void onSuccess() {
                    System.out.println("load binary success");
                }

                @Override
                public void onFinish() {
                    System.out.println("load binary finished");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            e.printStackTrace();
        }
    }


    /**
     * Invoke ffmpeg with the files as params
     */
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void execFFMpeg(){


        String outputName = txtOutputFile.getText().toString();
        if (outputName.trim().equals("")){

            new AlertDialog.Builder(this)
                    .setMessage(R.string.permission_denied_write)
                    .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            txtOutputFile.requestFocus();
                        }
                    }).show();
            return;

        }

        if (!outputName.endsWith(".mp4"))
            outputName += ".mp4";


        NotificationCenter center = NotificationCenter.getInstance();
        int id = center.getNextNotificationId();
        center.startConvertNotification(id, "In Queue", filePaths[0]);

        Intent ii = new Intent(this, ProcessingIntentService.class);
        ii.putExtra(ProcessingIntentService.KEY_FIRST_VIDEO, filePaths[0]);
        ii.putExtra(ProcessingIntentService.KEY_SECOND_VIDEO, filePaths[1]);
        ii.putExtra(ProcessingIntentService.KEY_AUDIO, filePaths[2]);
        ii.putExtra(ProcessingIntentService.KEY_NOTIFICATION_ID, id);
        ii.putExtra(ProcessingIntentService.KEY_OUTPUT_NAME, outputName);

        startService(ii);
    }


    /**
     * Permission related messages
     * @param request ()
     */
    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForStorage(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_rationale_write)
                .setPositiveButton(R.string.btn_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.btn_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                }).show();
    }

    /**
     * Permission related messages
     *
     */
    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_denied_write)
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }


    /**
     * Permission related messages
     *
     */
    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForStorage() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_never_ask_write)
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }


    /**
     * result from the pickers
     * @param requestCode ()
     * @param resultCode ()
     * @param data ()
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        filePicker.processResult(requestCode, resultCode, data);
    }




}
