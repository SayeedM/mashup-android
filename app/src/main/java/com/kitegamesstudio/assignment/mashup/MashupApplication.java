package com.kitegamesstudio.assignment.mashup;

import android.app.Application;
import android.os.StrictMode;

import com.kitegamesstudio.assignment.mashup.notification.NotificationCenter;

/**
 *
 * Created by sayeedm on 11/12/17.
 */

public class MashupApplication extends Application {

    public void onCreate(){
        super.onCreate();
        NotificationCenter.getInstance().init(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }
}
