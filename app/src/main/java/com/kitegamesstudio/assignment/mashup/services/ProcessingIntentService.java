package com.kitegamesstudio.assignment.mashup.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import com.kitegamesstudio.assignment.mashup.ffmpeg.MashupController;

/**
 * We run all our ffmpeg stuff in a background service
 * So that even when we go to background the operation runs
 * The user wont have to stay in the app to complete
 *
 * Created by sayeedm on 11/11/17.
 */
@SuppressWarnings("SpellCheckingInspection")
public class ProcessingIntentService extends IntentService {

    public static final String KEY_FIRST_VIDEO = "key_first_video";
    public static final String KEY_SECOND_VIDEO = "key_second_video";
    public static final String KEY_AUDIO = "key_audio";
    public static final String KEY_OUTPUT_NAME = "key_output_name";

    public static final String KEY_NOTIFICATION_ID = "notification_id";

    public ProcessingIntentService(){
        super("ProcessingIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            String firstVideo = intent.getStringExtra(KEY_FIRST_VIDEO);
            String secondVideo = intent.getStringExtra(KEY_SECOND_VIDEO);
            String audio = intent.getStringExtra(KEY_AUDIO);
            String output = intent.getStringExtra(KEY_OUTPUT_NAME);
            int notificationId = intent.getIntExtra(KEY_NOTIFICATION_ID, 0);

            String outputPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath() + "/" + output;
            MashupController controller = new MashupController(this, firstVideo, secondVideo, audio, outputPath, notificationId);
            controller.startMashup();
        }
    }
}
