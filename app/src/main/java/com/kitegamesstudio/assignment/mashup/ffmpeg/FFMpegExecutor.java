package com.kitegamesstudio.assignment.mashup.ffmpeg;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.kitegamesstudio.assignment.mashup.notification.NotificationCenter;
import java.io.File;
import java.util.concurrent.CountDownLatch;

/**
 *
 * The class which is responsible for running ffmpeg commands
 *
 * Created by sayeedm on 11/11/17.
 */
@SuppressWarnings({ "SpellCheckingInspection", "WeakerAccess" })
public class FFMpegExecutor {

    private Context context;
    private CountDownLatch latch = new CountDownLatch(1);
    private int notificationId = 0;

    public FFMpegExecutor(Context context, int notificationId){
        this.context = context;
        this.notificationId = notificationId;
    }

    public void await(){
        try{
            latch.await();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public void execute(final String [] cmdParts){
        final long start = System.currentTimeMillis();
        FFmpeg ffmpeg = FFmpeg.getInstance(context);
        try {

            ffmpeg.execute(cmdParts, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    NotificationCenter.getInstance().startConvertNotification(notificationId, "Mashin up", "");
                }

                @Override
                public void onProgress(String message) {
//                    System.out.println(message);
                }

                @Override
                public void onFailure(String message) {
                    NotificationCenter.getInstance().setUploadNotificationsFailed(notificationId, "Failed to mash", "");
                    System.out.println("failure " + message);
                }

                @Override
                public void onSuccess(String message) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(cmdParts[cmdParts.length - 1])), "video/mp4");

                    NotificationCenter.getInstance().setUploadNotificationsSuccess(notificationId, "Success", "", intent);
                }

                @Override
                public void onFinish() {
                    System.out.println("execution finished");
                    long end = System.currentTimeMillis();
                    System.out.println("Took Time " + (end - start));
                    latch.countDown();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }
}
