package com.kitegamesstudio.assignment.mashup.ffmpeg;

import android.content.Context;

/**
 *
 * The wrapper which works with FFMpeg executor
 * it basically provides and fabrocates command parameter according to business logic
 * and passes it to the next layer i.e. FFMpeg Executor
 *
 * Created by sayeedm on 11/11/17.
 */
@SuppressWarnings("SpellCheckingInspection")
public class MashupController {

    private String firstVideoPath, secondVideoPath, audioPath, outputPath;
    private Context context;
    private int notificationId;

    public MashupController(Context context, String firstVideoPath, String secondVideoPath, String audioPath, String outputPath, int notificationId){
        this.firstVideoPath = firstVideoPath;
        this.secondVideoPath = secondVideoPath;
        this.audioPath = audioPath;
        this.context = context;
        this.notificationId = notificationId;
        this.outputPath = outputPath;
    }

    public void startMashup(){
        long start = System.currentTimeMillis();

        String [] cmd = FFMpegCommandFactory.getMashupCommand(new String[]{firstVideoPath, secondVideoPath, audioPath}, outputPath);
        FFMpegExecutor firstExecutor = new FFMpegExecutor(context, notificationId);

        firstExecutor.execute(cmd);

        /* we run controller from background service, and we make the call blocking here */
        /* ffmpeg is already heavy enough, there is no point running multiple processing at */
        /* once in a mobile device */
        firstExecutor.await();

        long end = System.currentTimeMillis();

        System.out.println("Needed time : " + (end - start));
    }
}
