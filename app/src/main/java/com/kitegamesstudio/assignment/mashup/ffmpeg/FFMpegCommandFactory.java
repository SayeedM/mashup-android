package com.kitegamesstudio.assignment.mashup.ffmpeg;

/**
 *
 * Created by sayeedm on 11/11/17.
 */
@SuppressWarnings("SpellCheckingInspection")
class FFMpegCommandFactory {

    static String [] getMashupCommand(String [] inputPath, String outputPath){
        /* using a complex filter to run all the transformation in one go */
        return new String[] {
                "-i", inputPath[0],
                "-i", inputPath[1],
                "-i", inputPath[2],
                "-filter_complex", "[0:v:0]scale=640:640,setsar=1[v1];[1:v:0]scale=640:640,setsar=1[v2];[v1][v2]concat=n=2:v=1:a=0[v_out]",
                "-map", "[v_out]", "-map", "2:a", "-preset", "ultrafast", "-crf", "28",
                "-threads", "20", "-me_method", "zero",
                "-y", outputPath
        };

    }

    public static String [] getVersionCommand(){
        return new String[]{
                "-version"
        };
    }
}
