package com.kitegamesstudio.assignment.mashup.fs;

import java.io.File;

/**
 * Just a simple wrapper related to path(s) operations
 * Created by sayeedm on 11/10/17.
 */
@SuppressWarnings("SpellCheckingInspection")
public class Path {

    private File file;

    public Path(String absolutePath){
        file = new File(absolutePath);
    }

    public String getFileName(){
        return file.getName();
    }

}
