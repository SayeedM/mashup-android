package com.kitegamesstudio.assignment.mashup.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * manages status notifications, like video is being queued up for mashing,
 * video is being mashed right now, video mashing success or failure
 *
 * in case of clicking a "successful mashed" notification ax external intent is
 * being fired to view the vide
 *
 * Created by sayeedm on 1/3/17.
 */
@SuppressWarnings("SpellCheckingInspection")
public class NotificationCenter {

    private AtomicInteger latestNotificationId = new AtomicInteger(0);

    public int getNextNotificationId(){
        return latestNotificationId.incrementAndGet();
    }


    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mUploadNotificationBuilder;
    private Context context;

    public void init(Context context){
        mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mUploadNotificationBuilder = new NotificationCompat.Builder(context);
        this.context = context;
    }

    private static NotificationCenter instance = null;

    public static NotificationCenter getInstance(){
        if (instance == null)
            instance = new NotificationCenter();
        return instance;
    }

    private NotificationCenter(){

    }

    public void startConvertNotification(int notificationId, String title, String body){
        mUploadNotificationBuilder.setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(android.R.drawable.presence_video_busy);
        mUploadNotificationBuilder.setProgress(100, 0, true);
        mNotifyManager.notify(notificationId, mUploadNotificationBuilder.build());

    }


    public void setUploadNotificationsSuccess(int id, String title, String body, Intent ii){
        mUploadNotificationBuilder.setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(android.R.drawable.ic_menu_gallery);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                ii, 0);

        mUploadNotificationBuilder.setProgress(0, 0, false);
        mUploadNotificationBuilder.setContentIntent(intent);

        mNotifyManager.notify(id, mUploadNotificationBuilder.build());
    }

    public void setUploadNotificationsFailed(int id, String title, String body){
        mUploadNotificationBuilder.setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(android.R.drawable.stat_notify_error);
        mUploadNotificationBuilder.setProgress(0, 0, false);
        mNotifyManager.notify(id, mUploadNotificationBuilder.build());
    }
}

