package com.kitegamesstudio.assignment.mashup.fs;

import android.app.Activity;
import android.content.Intent;

import com.kbeanie.multipicker.api.AudioPicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.AudioPickerCallback;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;

import static android.app.Activity.RESULT_OK;

/**
 * The file picker wrapper, uses a 3rd party library underneath and
 * it pretty much sucks. Need to write one myself in future
 *
 * Created by sayeedm on 11/10/17.
 */
@SuppressWarnings("SpellCheckingInspection")
public class FilePicker {

    private Activity context;

    private VideoPicker videoPicker;
    private AudioPicker audioPicker;

    public FilePicker(Activity context){
        this.context = context;
    }

    public void pickVideo(VideoPickerCallback callback){
        if (videoPicker == null) {
            videoPicker = new VideoPicker(context);
            videoPicker.setDebugglable(true);
        }
        videoPicker.setVideoPickerCallback(callback);
        videoPicker.pickVideo();
    }

    public void pickAudio(AudioPickerCallback callback){
        if (audioPicker == null){
            audioPicker = new AudioPicker(context);
            audioPicker.setDebugglable(true);
        }
        audioPicker.setAudioPickerCallback(callback);
        audioPicker.pickAudio();
    }

    public void processResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK) {
            if(requestCode == Picker.PICK_VIDEO_DEVICE) {
                videoPicker.submit(data);
            } else if (requestCode == Picker.PICK_AUDIO){
                audioPicker.submit(data);
            }
        }
    }


}
